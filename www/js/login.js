$(document).ready(function() {
    login();

    function login() {
        if (window.localStorage.getItem("Login") !== null) {
            location.href = "dashboard.html";
        }
        if (isOnline() == false) {
            toastr.options = {
                "closeButton": true, // true/false
                "debug": false, // true/false
                "newestOnTop": false, // true/false
                "progressBar": false, // true/false
                "positionClass": "toast-bottom-right", // toast-top-right / toast-top-left / toast-bottom-right / toast-bottom-left
                "preventDuplicates": false, //true/false
                "onclick": null,
                "showDuration": "300", // in milliseconds
                "hideDuration": "1000", // in milliseconds
                "timeOut": "5000", // in milliseconds
                "extendedTimeOut": "1000", // in milliseconds
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["error"]("Error de Conexión, no estas conectado a internet");
            return false;
        }
        $("#login").parsley();
        var formCitaValid = $("form[name=formLogin]");
        formCitaValid.on('submit', function(e) {
            e.preventDefault();
            $("#preloading").show();
            var data = $(this).serialize() + "&tipo=2";
            $.ajax({
                url: 'https://www.simiinmobiliarias.com/base/simired/models/simidocs.php?data=11',
                type: 'post',
                dataType: 'json',
                data: data,
                beforeSend: function() {
                    // $(".preloader").show();
                },
                success: function(data) {
                    if (data.Error == 1) {
                        $("#preloading").hide();
                        toastr.options = {
                            "closeButton": true, // true/false
                            "debug": false, // true/false
                            "newestOnTop": false, // true/false
                            "progressBar": false, // true/false
                            "positionClass": "toast-bottom-right", // toast-top-right / toast-top-left / toast-bottom-right / toast-bottom-left
                            "preventDuplicates": false, //true/false
                            "onclick": null,
                            "showDuration": "300", // in milliseconds
                            "hideDuration": "1000", // in milliseconds
                            "timeOut": "5000", // in milliseconds
                            "extendedTimeOut": "1000", // in milliseconds
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                        toastr["error"](data.msn);
                    } else {
                        $("#preloading").hide();
                        toastr.options = {
                            "closeButton": true, // true/false
                            "debug": false, // true/false
                            "newestOnTop": false, // true/false
                            "progressBar": false, // true/false
                            "positionClass": "toast-bottom-right", // toast-top-right / toast-top-left / toast-bottom-right / toast-bottom-left
                            "preventDuplicates": false, //true/false
                            "onclick": null,
                            "showDuration": "300", // in milliseconds
                            "hideDuration": "1000", // in milliseconds
                            "timeOut": "5000", // in milliseconds
                            "extendedTimeOut": "1000", // in milliseconds
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                        toastr["success"](data.msn);
                        if (data.data[0]["inmobiliarias"].length == 1) {
                            saveLocalStorage(data.data);
                            setTimeout(function() {
                                location.href = "dashboard.html";
                            }, 2000);
                        } else {
                            // alert("se redigira eligiendo inmobiliaria");
                            $("#selectInmobiliaria").addClass("active");
                            $("#selectInmobiliaria .promptConfirmContainer__action").html("");
                            var html = "";
                            $.each(data.data[0]["inmobiliarias"], function(index, val) {
                                var extPng = data.data[0]["inmobiliarias"][index].logo.indexOf(".png");
                                var extJpg = data.data[0]["inmobiliarias"][index].logo.indexOf(".jpg");
                                var extGif = data.data[0]["inmobiliarias"][index].logo.indexOf(".gif");
                                if (extPng != -1 || extJpg != -1 || extGif != -1) {
                                    var img = '<img src="' + data.data[0]["inmobiliarias"][index].logo + '" alt="' + data.data[0]["inmobiliarias"][index].nomInmo + '">';
                                } else {
                                    var img = data.data[0]["inmobiliarias"][index].nomInmo;
                                }
                                html += '<a class="promptConfirmContainer__action--confirm dinamicButton" title="'+data.data[0]["inmobiliarias"][index].nomInmo+'" id="monthPremium" data-id="' + data.data[0]["inmobiliarias"][index].inmo + '"><div class="table"><div class="tablecell">' + img + '</div></div></a>';
                            });
                            $("#selectInmobiliaria .promptConfirmContainer__action").prepend(html);
                        }
                        saveLocalStorage(data.data);
                        /*setTimeout(function(){
                                        location.href = "dashboard.html";
                                    },2000);*/
                    }
                }
            });
        });
    }

    function saveLocalStorage(data) {
        if (typeof(Storage) !== "undefined") {
            window.localStorage.setItem("Login", data[0].cuenta);
            window.localStorage.setItem("cuenta", data[0].cuenta);
            window.localStorage.setItem("nit", data[0].nit);

            var logo = data[0].inmobiliarias[0].logo;
            var extPng = logo.indexOf(".png");
            var extJpg = logo.indexOf(".jpg");
            var extGif = logo.indexOf(".gif");
            if (extPng != -1 || extJpg != -1 || extGif != -1) {
                var img = '<img class="img-responsive" src="' + logo + '" alt="' + data[0].inmobiliarias[0].nomInmo + '">';
            } else {
                var img = data[0].inmobiliarias[0].nomInmo;
            }
            

            window.localStorage.setItem("Foto", img);
            window.localStorage.setItem("inmo", data[0].inmobiliarias[0].inmo);
            window.localStorage.setItem("nomInmo", data[0].inmobiliarias[0].nomInmo);
            window.localStorage.setItem("nombre", data[0].nombre);
            window.localStorage.setItem("direccion", data[0].direccion);
        } else {
            console.log("Sorry, your browser does not support Web Storage...");
        }
    }

    function isOnline() {
        var online = navigator.onLine; // Detecting the internet connection
        if (online) {
            return true;
        } else {
            return false;
        }
    }

    function confirmPrompt() {
        $(document).on("click", "#selectInmobiliaria .dinamicButton", function(e) {
            // effectRipple("#confirmPrompt");
            //deleteLocalStorage();
            window.localStorage.setItem("inmo", $(this).data("id"));

            if($(this).find("img").length == 0){

                 var img = $(this).attr("title");
                window.localStorage.setItem("Foto", img);
                location.href = "dashboard.html";

            }else{

                var logo = $(this).find("img").attr("src");
                var extPng = logo.indexOf(".png");
                var extJpg = logo.indexOf(".jpg");
                var extGif = logo.indexOf(".gif");
                if (extPng != -1 || extJpg != -1 || extGif != -1) {
                    var img = '<img class="img-responsive" src="' + logo + '" alt="' + $(this).find("img").attr("alt") + '">';
                } else {
                    var img = $(this).attr("title");
                }
                window.localStorage.setItem("Foto", img);
                // setTimeout(function(){
                location.href = "dashboard.html";

            }
            
            // },2000);
            // navigator.app.exitApp();
        });
        $(document).on("click", "#negationPrompt", function(e) {
            effectRipple("#negationPrompt");
            parentClosed = $(this).data("parent");
            $(parentClosed + " .promptConfirmContainer").addClass('bounceOutDown');
            setTimeout(function() {
                $(parentClosed + " .promptConfirmContainer").removeClass('bounceOutDown');
                $(parentClosed).removeClass('active');
            }, 1000)
        });
    }
    confirmPrompt();
});