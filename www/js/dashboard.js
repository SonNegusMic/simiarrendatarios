var navigation = $("body").data("navigation");

 $(function(){
 	/*********************************
 	 *              Cargamos datos iniciales a mostrar               *
 	 *********************************/
 	function loadInfo(){
        if((window.localStorage.getItem("inmo") == undefined)  ){
            //location.href = "index.html";
            return false;
        }else{
          //../../models/simidocs.php?data=1
          $("#preloading").removeClass("hidden");

          $.ajax({
            url: 'https://www.simiinmobiliarias.com/base/simired/models/simidocs.php?data=1',
            type: 'POST',
            dataType: 'json',
            "data": {inmo:window.localStorage.getItem("inmo")},
          })
          .done(function(data) {
            $("#preloading").addClass("hidden");  
            saveLocalStorage(data.data);
            
          });
        }
        $(".header--minimalMenu__info h3 span").html(window.localStorage.getItem("nombre"));

          var logo = window.localStorage.getItem("Foto");

          //alert(logo);
          var extPng = logo.indexOf(".png");
          var extJpg = logo.indexOf(".jpg");
          var extGif = logo.indexOf(".gif");

          if (extPng != -1 || extJpg != -1 || extGif != -1) {
              var img = logo//'<img class="img-responsive" src="' + logo + '" alt="' + $(this).find("img").attr("alt") + '">';
          } else {
              var img = window.localStorage.getItem("Foto");
          }
              
        $(".header--minimalMenu__image").html(img);

        if(window.localStorage.getItem("inmo")){
            $(".consolidadoIngresos").show();
        }
        
    }

  function saveLocalStorage(data) {
      if (typeof(Storage) !== "undefined") {
          window.localStorage.setItem("antigua", data.configInmo.inmobiliaria.antigua);
          window.localStorage.setItem("cfa", data.configInmo.inmobiliaria.cfac);
         // alert(window.localStorage.getItem("antigua"));
      } else {
          console.log("Sorry, your browser does not support Web Storage...");
      }
  }
/*********************************
 *              efecto onda en elementos               *
 *********************************/
 	function effectRipple(element){
        // Remove any old one
        $("#overlayGlobal").show();
          $(".ripple").remove();
          $(".rippleInverse").remove();
          var element = element;

          // Setup
          var posX = $(element).offset().left,
              posY = $(element).offset().top,
              buttonWidth = $(element).width(),
              buttonHeight =  $(element).height();
          
          // Add the element
          $(element).prepend("<span class='ripple'></span>");

          
         // Make it round!
          if(buttonWidth >= buttonHeight) {
            buttonHeight = buttonWidth;
          } else {
            buttonWidth = buttonHeight; 
          }
          
          // Get the center of the element
          var x = element.pageX - posX - buttonWidth / 2;
          var y = element.pageY - posY - buttonHeight / 2;
          
         
          // Add the ripples CSS and start the animation
          $(".ripple").css({
            width: buttonWidth,
            height: buttonHeight,
            top: y + 'px',
            left: x + 'px'
          }).addClass("rippleEffect");
          setTimeout(function(){
          $("#overlayGlobal").hide();
        },2000);
    }
/*********************************
 *              efecto onda inverso en elementos               *
 *********************************/
    function effectRippleInverse(element){
        // Remove any old one
         $("#overlayGlobal").show();
          $(".ripple").remove();
          $(".rippleInverse").remove();
          var element = element;

          // Setup
          var posX = $(element).offset().left,
              posY = $(element).offset().top,
              buttonWidth = $(element).width(),
              buttonHeight =  $(element).height();
          
          // Add the element
          $(element).prepend("<span class='rippleInverse'></span>");

          
         // Make it round!
          if(buttonWidth >= buttonHeight) {
            buttonHeight = buttonWidth;
          } else {
            buttonWidth = buttonHeight; 
          }
          
          // Get the center of the element
          var x = element.pageX - posX - buttonWidth / 2;
          var y = element.pageY - posY - buttonHeight / 2;
          
         
          // Add the ripples CSS and start the animation

          $(".rippleInverse").css({
            width: buttonWidth,
            height: buttonHeight,
            top: y + 'px',
            left: x + 'px'
          }).addClass("rippleEffectInverse");

          setTimeout(function(){
            $(".rippleInverse").remove();
            
          },500);

          setTimeout(function(){
            $("#overlayGlobal").hide();
          },500);

       
    }

    /*********************************
     *              abre el menu               *
     *********************************/

 	function openMenu(){

        $(document).on("click","#openMenu",function(e){
            e.preventDefault();
            $(".menu-dash").addClass("active");
            $(".touch").addClass("active");
            // effectRipple("#overlayGlobal");
        });

    }

/*********************************
 *              abre la modal de about us               *
 *********************************/
    function aboutUs(){

       $(document).on("click",".aboutUs",function(e){
        e.preventDefault();
          
          $("#aboutUs").modal("show");
      });

    }
/*********************************
 *              abrimos menu                *
 *********************************/
    function swipeMenu(){

        $(".touch,.menu-dash").swipe({

            click:function(event,target){
                if($(".touch").hasClass("active")){

                    // effectRippleInverse("#overlayGlobal");

                }
                $(".menu-dash").removeClass("active");
                $(".touch").removeClass("active");
                $(".closeButton .c-hamburger").removeClass('is-active');

            },    

            swipeStatus:function(event, phase, direction, distance, duration, fingers)

              {

                if (phase=="move" && direction =="right") {

                   $(".menu-dash").addClass("active");
                   $(".touch").addClass("active");
                   $(".closeButton .c-hamburger").addClass('is-active');
                   // effectRipple("#overlayGlobal");
                   //console.log(distance);

                   return false;

                }

                if (phase=="move" && direction =="left") {
                  //console.log(distance);
                   $(".menu-dash").removeClass("active");
                   $(".touch").removeClass("active");
                   $(".closeButton .c-hamburger").removeClass('is-active');
                   // effectRippleInverse("#overlayGlobal");
                   return false;

                }

                //console.log(event);

              }

          });

    }

    function tracking(){
        $(document).on("click",".tracking",function(e){
            $("body").data("navigation",$(this).data("track"));
            navigation = $(this).data("track");
            //alert($("body").data("navigation"));
        });

        $('#aboutUs').on('hidden.bs.modal', function () {
           // alert("cerrando modal");
            $("body").data("navigation",0)
        });
    }

    /*********************************
     *              Aplicamos filtro de busqueda               *
     *********************************/
    function aplicarFiltro(){
        $(document).on("click","#aplicarFiltro",function(e){
            e.preventDefault();
            var tGrafico = 0;
            var cod = 0;
            var title = '';
            if( $("body").data("navigation") ){

            }

            loadFacturas();

            $.each($(".mainMenu .openModal"), function(index, val) {
               if($(this).data("target")=="#facturas" && !$(this).hasClass('active')){
                  $(this).trigger('click');
               }  
            });

            //$(".openModal:eq(0)").trigger('click');
           
            $("#windowFilter").removeClass("active");
        });
    }
    /*********************************
     *              funcion para confirmar cierre de la aplicacion o cancelacion de la modal
     *********************************/
     function confirmPromptApp(){

        $(document).on("click","#confirmPrompt",function(e){

            // effectRipple("#confirmPrompt");
            //deleteLocalStorage();
            navigator.app.exitApp();
            
        });

        $(document).on("click","#negationPromptCloseApp",function(e){

            effectRipple("#negationPrompt");
            parentClosed = $(this).data("parent");
            $(parentClosed+" .promptConfirmContainer").addClass('bounceOutDown');
            setTimeout(function(){
            $(parentClosed+" .promptConfirmContainer").removeClass('bounceOutDown');
            $(parentClosed).removeClass('active');
            },1000)
        });





        
    }
    /*********************************
     *              borramos la cache               *
     *********************************/
    function deleteLocalStorage(){
        window.localStorage.removeItem("Login");
        window.localStorage.removeItem("cuenta");
        window.localStorage.removeItem("nit");
        window.localStorage.removeItem("Foto");
        window.localStorage.removeItem("inmo");
        window.localStorage.removeItem("nomInmo");
        window.localStorage.removeItem("nombre");
        window.localStorage.removeItem("direccion");
        location.href = "index.html"
    }
    /*********************************
     *              capturamos click del boton de filtro de busqueda               *
     *********************************/
    function filterButton(){
        $(document).on("click","#filterButton",function(e){
            e.preventDefault();
            $("#windowFilter").toggleClass("active");
            /*$(".summaryGhaph").removeClass("active");
            $(".openGrafica").removeClass("active");
            $(".chartGhaph").removeClass("active");*/

            if($("#windowFilter").hasClass('active')){
                // effectRipple("#overlayGlobal");
            }else{
               // effectRipple(".bgimg");
               // effectRippleInverse("#overlayGlobal");
            }
        });

    }
    /*********************************
     *              boton de cierre de session               *
     *********************************/
    function signupButton(){
        $(document).on("click",".signupButton",function(e){
            e.preventDefault();
            deleteLocalStorage();
        });
    }

    function closeCurrent(){
        $(document).on("click",".closeCurrent",function(e){
            e.preventDefault();
            $(this).closest('.active').removeClass('active');
        });
    }

    /*********************************
     *              funcion para retornar la fecha actual en formato yyyy-mm-dd               *
     *********************************/

    function nowDate(){

        var date = new Date;
        var yyyy = date.getFullYear().toString();                                    
        var mm = (date.getMonth()+1).toString(); // getMonth() is zero-based         
        var dd  = date.getDate().toString();             
                            
        return yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]);

    }

    /*********************************
     *              abrir modales con tablas               *
     *********************************/

    function openModal(){
    	$(document).on("click",".openModal",function(e){
    		e.preventDefault();
    		$($(this).data("target")).toggleClass("active");
        if($(this).data("target")=="#facturas"){
          loadFacturas();
        }

        if($(this).data("target")=="#selectInmobiliaria"){
          loadInmobiliarias();
          loadInmo();
        }
    		$(this).toggleClass("active");
    		// effectRipple("#overlayGlobal");
    	});
    }

    function selectMaterial(method){
    	var date = new Date;
    	var now = date.getFullYear().toString();
        var init = now-4;    

        for(var i=init;i<=now;i++){
        	$(".selectGrafica").append("<option value='"+i+"'>"+i+"</option>");
        }
             
         $('.selectGrafica').material_select();
       
    }

  /*********************************
   *              validamos si tiene o no cupon de codigo de barras               *
   *********************************/
   function loadCupon(){

    var anio = $("#selectAnio option:selected").val();
    var date = new Date;
    if(anio == 0){
      anio = date.getFullYear();
    }

    

    $.ajax({
      url: 'https://www.simiinmobiliarias.com/base/simired/models/simidocs.php?data=9',
      type: 'POST',
      dataType: 'json',
      "data": {inmo:window.localStorage.getItem("inmo"),nit:window.localStorage.getItem("nit"),anio:anio},
    })
    .done(function(data) {
      if (data.length != 0){ // Any scope

        /*$("#downloadCupon").attr("href",data[0].Ruta);
        $(".btnBarr").hide();
        $(".barrEnabled").show();*/
        $("#buttonsCupon").html("");
        $.each(data, function(index, val) {

          $("#buttonsCupon").append('<a class="btn btn-default btn-lg btn-block btnBarr barrEnabled" href="'+data[index].Ruta+'">\
                            <i aria-hidden="true" class="fa fa-download">\
                            </i>\
                            Descargue su cupón de pago Aquí\
                            <br>\
                                Inmueble:\
                                <span>\
                                    '+data[index].Inmueble+'\
                                </span>\
                            </br>\
                        </a>');
           /*$("#buttonsCupon").append('<div class="button">\
              <a href="'+data[index].Ruta+'" target="_blank" id="downloadCupon">Descargue su cupón de pago Aquí</a>\
              <p class="top">Inmueble: <span>'+data[index].Inmueble+'</span></p>\
              <p class="bottom">Inmueble: <span>'+data[index].Inmueble+'</span></p>\
            </div>');*/
           
        });

      }else{
        $(".btnBarr").hide();
        $(".button").hide();
        $(".bg-danger").show();
      }
      
    });


  }

  function loadFacturas(){
    var anio = $("#selectAnio option:selected").val();
    var date = new Date;
    if(anio == 0){
      anio = date.getFullYear();
    }
    $("#preloading").removeClass("hidden");
    $("#listadoFacturas").html('');
    $("#preloading").addClass("zindexmax");
     $.ajax({
        url: 'https://www.simiinmobiliarias.com/base/simired/models/simidocs.php?data=8',
        type: 'POST',
        dataType: 'json',
        "data": {inmo:window.localStorage.getItem("inmo"),nit:window.localStorage.getItem("nit"),anio:anio,antigua:window.localStorage.getItem("antigua"),cfac:window.localStorage.getItem("cfa")},
      })
      .done(function(data) {

        

        $.each(data, function(index, val) {

          if(data[index].filefound == 0 ){
            var download = '<i aria-hidden="true" class="fa fa-exclamation-triangle"></i>';
          }else{
            var download = ' <a href="'+data[index].Ruta2+'">\
                                    <i aria-hidden="true" class="fa fa-download">\
                                    </i>\
                                </a>';
          }
           $("#listadoFacturas").html('');
           var html ='<div class="blockquote-box clearfix">\
                            <div class="square pull-left">\
                               '+download+'\
                            </div>\
                            <div class="fecha">\
                                <b>\
                                    Fecha:\
                                </b>\
                                '+data[index].Fecha+'\
                            </div>\
                            <div class="fecha">\
                                <b>\
                                    Factura:\
                                </b>\
                                '+data[index].Factura+'\
                            </div>\
                            <span class="inmueble">\
                                <b>\
                                    Inmueble:\
                                </b>\
                                '+data[index].inmu+'\
                            </span>\
                            <div class="direccion">\
                                <b>\
                                    Dirección:\
                                </b>\
                                '+data[index].Direccion+'\
                            </div>\
                        </div>';
            $("#listadoFacturas").append(html);
        });
        $("#preloading").addClass("hidden");
        $("#preloading").removeClass("zindexmax");

      });
  }

  function loadInmobiliarias(){
    $("#preloading").removeClass("hidden");
    $("#listadoFacturas").html('');
    $("#preloading").addClass("zindexmax");
     $.ajax({
        url: 'https://www.simiinmobiliarias.com/base/simired/models/simidocs.php?data=12',
        type: 'POST',
        dataType: 'json',
        "data": {nit:window.localStorage.getItem("nit"),tipo:2},
      })
      .done(function(data) {

        var html = "";
        $("#selectInmobiliaria .promptConfirmContainer__action .dinamicButton:not(.negationPrompt)").remove();
        $.each(data, function(index, val) {
            var extPng = data[index].logo.indexOf(".png");
            var extJpg = data[index].logo.indexOf(".jpg");
            var extGif = data[index].logo.indexOf(".gif");
            if (extPng != -1 || extJpg != -1 || extGif != -1) {
                var img = '<img src="' + data[index].logo + '" alt="' + data[index].nomInmo + '">';
            } else {
                var img = data[index].nomInmo;
            }
            html += '<a class="promptConfirmContainer__action--confirm dinamicButton" id="monthPremium" title="'+data[index].nomInmo+'" data-id="' + data[index].inmo + '"><div class="table"><div class="tablecell">' + img + '</div></div></a>';
        });
        $("#selectInmobiliaria .promptConfirmContainer__action").prepend(html);

        $("#preloading").addClass("hidden");
        $("#preloading").removeClass("zindexmax");

      });
  }

  function confirmPrompt() {
      $(document).on("click", "#selectInmobiliaria .dinamicButton", function(e) {
          // effectRipple("#confirmPrompt");
          //deleteLocalStorage();
          window.localStorage.setItem("inmo", $(this).data("id"));

          if($(this).find("img").length == 0){
            var img = $(this).attr("title");
            $(".header--minimalMenu__image").html(img);
            window.localStorage.setItem("Foto", img);
            location.reload();
            
          }else{

            var logo = $(this).find("img").attr("src");
            var extPng = logo.indexOf(".png");
            var extJpg = logo.indexOf(".jpg");
            var extGif = logo.indexOf(".gif");
            if (extPng != -1 || extJpg != -1 || extGif != -1) {
                var img = '<img class="img-responsive" src="' + logo + '" alt="' + $(this).find("img").attr("alt") + '">';
            } else {
                var img = $(this).attr("title");
            }
            $(".header--minimalMenu__image").html(img);
            window.localStorage.setItem("Foto", img);
            location.reload();
          }
          //alert($(this).data("id"));
          //alert(window.localStorage.getItem("inmo"));
         // loadInfo();
          // setTimeout(function(){
         
          // },2000);
          // navigator.app.exitApp();
      });
      $(document).on("click", "#negationPrompt", function(e) {
          effectRipple("#negationPrompt");
          parentClosed = $(this).data("parent");
          $(parentClosed + " .promptConfirmContainer").addClass('bounceOutDown');
          setTimeout(function() {
              $(parentClosed + " .promptConfirmContainer").removeClass('bounceOutDown');
              $(parentClosed).removeClass('active');
              $(".openModal").removeClass("active");
          }, 1000)
      });
  }
  

  function resetPassword(){

    $("#updatePass").on("submit",function(e){
      e.preventDefault();
      $("#preloading").removeClass("hidden");
      $("#listadoFacturas").html('');
      $("#preloading").addClass("zindexmax");
      var data = $("#updatePass").serialize()+"&tipo=2&inmo="+window.localStorage.getItem("inmo")+"&nit="+window.localStorage.getItem("nit");
      $("#updatePass").parsley();

      $.ajax({
        url: 'https://www.simiinmobiliarias.com/base/simired/models/simidocs.php?data=13',
        type: 'POST',
        dataType: 'json',
        "data": data,
      })
      .done(function(data) {

          toastr.options = {
              "closeButton": true, // true/false
              "debug": false, // true/false
              "newestOnTop": false, // true/false
              "progressBar": false, // true/false
              "positionClass": "toast-bottom-right", // toast-top-right / toast-top-left / toast-bottom-right / toast-bottom-left
              "preventDuplicates": false, //true/false
              "onclick": null,
              "showDuration": "300", // in milliseconds
              "hideDuration": "1000", // in milliseconds
              "timeOut": "5000", // in milliseconds
              "extendedTimeOut": "1000", // in milliseconds
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          }
        if(data.Error == 0){

          toastr["success"](data.msn);
          $("#changePass").modal('hide');

        }else{
          toastr["error"](data.msn);
        }
        

        $("#preloading").addClass("hidden");
        $("#preloading").removeClass("zindexmax");

      });
    });

    $(document).on("click","#resetPassword",function(e){
      e.preventDefault();
      $("#updatePass").submit();
      
    });
  }

	openMenu();
	aboutUs();
	swipeMenu();  
	loadInfo();
	tracking();
	aplicarFiltro();
	confirmPrompt();
	filterButton();
	signupButton();
	closeCurrent();
	openModal();
	selectMaterial();
  loadCupon();
  confirmPromptApp();
  resetPassword();


 });